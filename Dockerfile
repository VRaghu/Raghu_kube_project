FROM golang:1.9-alpine

COPY *.go /go/
COPY configureCluster.sh /tmp/configureCluster.sh
RUN go build -o /deals

CMD /deals
EXPOSE 8080